# Oct_long_2020

**Total Numbers of problems : 9**

**No of problems solved : 3**

## Index 

 1. [X] [Covid Run](notes/CVDRUN.md)
 2. [X] [Chef and easy quries](notes/CHEFEZQ.md)
 3. [X] [Positive AND](notes/POSAND.md)
 4. [ ] [Replace for X](./notes/REPLESX.md)
 5. [ ] [[D-Dimensional MST]]
 6. [ ] [[Adding squares]]
 7. [ ] [[Random Knapsack]]
 8. [ ] [[Compress all subsegments]]
 9. [ ] [[Village Road Network]]
