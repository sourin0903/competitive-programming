# Replace X

### Problem Statement Summmary :

 - There is an array of n integers.
 - In an specific operation we can replace the kth smallest no of
   the array with ant number of our choice.
 - Find the minimtum no of a specific operation is needed to
   perform such that the pth smallest no of the array becomes X
   or if not possible print -1.
   
### Input :

 - first line contains the no of testcases.
 - For each testcase there are 2 lines:
     - First line contains three integers n, x, p, k.
     - Second line contains the n integers which denotes the
       array.
       
### Output :

 - Minimum no of operations needed.
 - If not possible print -1.

### Observations :

 1. If p<k and X > a[p] then it is not possible.
 2. If p<k and X<a[p] then it is possible and ans = p - position of x + 1.
 3. If p>k and x>a[p] then it is possible and ans = position of x - p + 1.
 4. If p>k and x<a[p] then it is not possible.
 5. 
