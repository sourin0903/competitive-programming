# Covid Run

### Problem Statement Summary :

 - There are n cities, arranged in cyclic order i.e. city no 1 is connected to 2; 2 is connected to 3, .... n is connected to 1.
 - Virus is currently at city no x.
 - Virus jumps k cities right in one move i.e. if it is at city x then it will go to city no x+k.
 - Cities in between x and x+k don't get infected.
 - Find if city no Y can be infected.

### Input :
        
 - First line contains the number of testcase.
 - For each testcase there are only one line :
     - Four integers denoting n,k,x,y.
    
### Output :
    
 - Print "YES" if Y can be affected otherwise print "NO".

### Main Observations :

 - If gcd(x,y)==1 then city will be affected by covid.
 - If not then there are some specific cities which will be affected by covid.
        
### Approach :

 - Create an arrary to keep a track of the cities that are visited.
 - Whenever we revisit a visited city we come out from the loop.
 - At the same time if we visit the 'y' city then we print 'YES'.
 - If we get no such instances then we print "NO".

### Code :

    [See the link](../codes/CVDRUN.cpp)
    
    
