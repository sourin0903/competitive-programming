# POSITIVE AND

### Problem Statement Summary :

 - For a given integer N find a permutation of the set {1,2,3,.....,N} such that for every i (i>0 && i<N) (Pi & Pi+1)>0.
        
### Input :
 - First line has one integer denoting the number of testcases.
 - For each testcases there is one integer N.
    
### Output :
 - If such permutation is possible then print the permutation else print -1.
            
### Observations :

 - For any positive integer i, if a = 2^i and b<a then, a&b=0 and b&a=0
 - So if N = 2^i, then it is never possible to form such permutation.
 - For example:
     - For N=1 => [1]
     - For N=2 => -1
     - For N=3 => [1,3,2] OR [2,3,1]
     - For N=4 => -1
     - For N=5 => [2,3,1,5,4]
     - For N=6 => [2,3,1,5,4,6]
     - For N=7 => [2,3,1,5,4,6,7]
     - For N=8 => -1
