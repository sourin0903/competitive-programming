# Chef and easy quries

### Problem Statement Summary :

 - Chef can answer atmost k queries in a day.
 - He receives Qi queries on ith day.
 - If Qi is greater than k then Qi-k number of quries added to
   the next day quries.
 - Find out the first day where Qi<k.

### Input :
 - he first line contains the no of testcases.
  - Each testcase contains two lines :
  - First line contains two integers denoting the no of
    days n and the value of k.
  - Second line contains the n integers denoting the
values of Qi.

### Output :
 -   Single integer denoting the first day where Qi<k.

### Approach :

-   We apply a brute force approach.
-   We keep a track of queries of each day.
-   If Qi is > k then the remaining queries will add to the next day.
-   So we can say that on a given day no of quries that has to be
    answered is Qi + left over from the previous day.
-   If Qi < k then chef will have some free time i.e. we have to
    print the number of that day.
-   After n days if there exists some leftover then chef needs
    extra rem/k days where he will get no free time so our ans in
    that case will be n + rem/k + 1. 
