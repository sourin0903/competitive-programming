/*
        Problem Name: "Covid Run"
        Problem_source: "Codechef"
        Author: Soumava Seal
        Date:"3rd Oct'2020"
*/

#include<bits/stdc++.h>
using namespace std;

#define INF 10000000000
#define ii pair<int,int>
#define li long int
#define lli long long int
#define vi vector<int>
#define vii vector<ii>
#define vlli vector<lli>
#define pi priority_queue<int>
#define pii priority_queue<ii>
#define FOR(i,n) for(int i=0;i<n;i++)
#define pb push_back
#define ff first
#define ss second

#define endl '\n'
const double PI = 3.141592653589793;
const double deg2rad = 0.017453292519943295;

lli modulo(lli a, lli b, lli n){
        long long x=1, y=a; 
        while (b > 0) {
                if (b%2 == 1) {
                        x = (x*y) % n;
                }
            y = (y*y) % n;
            b /= 2;
        }
        return x % n;
}

void solve(int n, int k, int x, int y){
    int a[n] = {0};
    int currPos = x-1;
    vi v;
    while(1){
        if(a[currPos] == 1) break;
        else{
            a[currPos] = 1;    
            if(currPos + 1 == y){
                cout<<"YES"<<endl;
                return;
            }
        }
        currPos = (currPos + k)%n; 
    }
    cout<<"NO"<<endl;
    return;
}

int main(){
        ios_base::sync_with_stdio(false);
        cin.tie(NULL);
        int t;
        cin>>t;
        while(t--){
            int n, k, x, y;
            cin>>n>>k>>x>>y;
            solve(n,k,x,y);
        }
}
