/*
    Problem Name :'Replace X'
    Problem Source : "codechef"
    Author :Soumava Seal
    Date :"8th Aug'2020"
*/

#include<bits/stdc++.h>
using namespace std;

#define INF 10000000000
#define ii pair<int, int>
#define li long int
#define lli long long int
#define vi vector<int>
#define vii vector<ii>
#define vlli vector<lli>
#define pi priority_queue<int>
#define pii priority_queue<ii>
#define pb push_back
#define ff first
#define ss second

#define endl '\n'
const double PI = 3.141592653589793;
const double deg2rad = 0.017453292519943295;

lli modulo(lli a, lli b, lli n){
    long long x=1, y=a;
    while(b>0){
        if(b%2 ==1) x = (x*y)%n;
        y = (y*y)%n;
        b /= 2;
    }

    return x%n;
}

int pos(int x, int a[], int n){
    int i = 0;
    while(i<n && a[i]<=x) i++;
    return i+1;
}

void solve(int n, int x, int p, int k, int a[]){
    
    sort(a,a+n);

    // FIXME: what happen if a[p-1]==x 

    if(p<k && x>a[p-1]){
        cout<<-1<<endl;
        return;
    }

    if(p<k && x<a[p-1]){
        int posX = pos(x,a,n);
        cout<<p - posX + 1<<endl;
        return;
    }

    if(p>k && x<a[p-1]){
        cout<<-1<<endl;
        return;
    }

    if(p>k && x>a[p-1]){
        int posX = pos(x,a,n);
        cout<<posX - p + 1<<endl;
        return;
    }

    if(p==k){
        int posX = pos(x,a,n);
        int ans = 0;
        if(posX>p) ans = posX - p + 1;
        else ans = p - posX + 1; 
        cout<<ans<<endl;
        return;
    }
}

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    int t;
    cin>>t;
    while(t--){
        int n, x, p, k;
        cin>>n>>x>>p>>k;
        int a[n];
        for (int i = 0; i < n; i++) {
            cin>>a[i];
        }
        solve(n,x,p,k,a);
    }
}

