/*
        Problem Name :'Positive AND'
        Problem Source : "codechef"
        Author :Soumava Seal
        Date :"5th Oct'2020"
*/

#include<bits/stdc++.h>
using namespace std;

#define INF 10000000000
#define ii pair<int, int>
#define li long int
#define lli long long int
#define vi vector<int>
#define vii vector<ii>
#define vlli vector<lli>
#define pi priority_queue<int>
#define pii priority_queue<ii>
#define pb push_back
#define ff first
#define ss second

#define endl '\n'
const double PI = 3.141592653589793;
const double deg2rad = 0.017453292519943295;

lli modulo(lli a, lli b, lli n){
        long long x=1, y=a;
        while(b>0){
                if(b%2 ==1) x = (x*y)%n;
                y = (y*y)%n;
                b /= 2;
        }

        return x%n;
}

bool power(int n){
    int a = 2;
    while(a<=n){
        if(a == n) return true;
        a *= 2;
    }

    return false;
}

void solve(int n){
    if(n==1){
        cout<<1;
        return;
    }

    if(power(n)){
        cout<<-1;
        return;
    }
    cout<<2<<" 3"<<" 1 ";

    for(int i=4; i<=n;){
        if(power(i)){
            cout<<i+1<<" "<<i<<" ";
            i += 2;
        }else{
            cout<<i<<" ";
            i++;
        }
    }
}

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    int t;
    cin>>t;
    while(t--){
        int n;
        cin>>n;
        solve(n);
        cout<<endl;
    }
}


