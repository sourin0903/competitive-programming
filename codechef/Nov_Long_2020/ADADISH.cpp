/*
Problem Name: 'Ada and Dishes'
Author Name: Soumava Seal
Problem Source: 'Codechef'
Date: '9th Nov' 2020'
----------------------------------------------
*/

#include<bits/stdc++.h>
using namespace std;

#define INF 10000000000
#define ii pair<int,int>
#define li long int
#define lli long long int
#define vi vector<int>
#define vii vector<ii>
#define vlli vector<lli>
#define pi priority_queue<int>
#define pii priority_queue<ii>
#define FOR(i,n) for(int i=0;i<n;i++)
#define pb push_back
#define ff first
#define ss second

#define endl '\n'
const double PI = 3.141592653589793;
const double deg2rad = 0.017453292519943295;

void solve(int n, int c[]){
    sort(c, c+n);

    if(n==1) cout<<c[0]<<endl;
    else if(n==2) cout<<max(c[0], c[1])<<endl;
    else if(n == 3) cout<<max((c[0]+c[1]), c[2])<<endl;
    else{
        int pos1 = max((c[0]+c[1]+c[2]), c[3]);
        int pos2 = max((c[0]+c[3]), (c[2]+c[1]));

        cout<<min(pos1, pos2)<<endl;
    }
    
}

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    int t;
    cin>>t;
    while(t--){
        // Write your code here for each testcase
        int n, c[5] = {0};
        cin>>n;
        FOR(i,n) cin>>c[i];
        solve(n,c);
    }
}