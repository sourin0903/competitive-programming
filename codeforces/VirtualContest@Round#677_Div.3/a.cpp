/*
    Problem Name : 
    Problem Source : Codeforces
    Author :Soumava Seal
    Date : October 22, 2020
*/

#include<bits/stdc++.h>
using namespace std;

#define INF 10000000000
#define ii pair<int, int>
#define li long int
#define lli long long int
#define vi vector<int>
#define vii vector<ii>
#define vlli vector<lli>
#define pi priority_queue<int>
#define pii priority_queue<ii>
#define pb push_back
#define ff first
#define ss second

#define endl '\n'
const double PI = 3.141592653589793;
const double deg2rad = 0.017453292519943295;

lli modulo(lli a, lli b, lli n){
    long long x=1, y=a;
    while(b>0){
        if(b%2 ==1) x = (x*y)%n;
        y = (y*y)%n;
        b /= 2;
    }

    return x%n;
}

int a[4] = {1,3,6,10};

void solve(int x){

    int d = x%10;
    
    int len = 0;

    while(x>0){
        len++;
        x /= 10;
    }

    int ans = 10*(d-1) + a[len-1];

    cout<<ans<<endl;
    
}

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);

    int t;
    cin>>t;
    while(t--){
        int x;
        cin>>x;
        solve(x);
    }
}
