/*
    Problem Name : Dominant Piranha
    Problem Source : codeforces
    Author :Soumava Seal
    Date : October 24, 2020
*/

#include<bits/stdc++.h>
using namespace std;

#define INF 10000000000
#define ii pair<int, int>
#define li long int
#define lli long long int
#define vi vector<int>
#define vii vector<ii>
#define vlli vector<lli>
#define pi priority_queue<int>
#define pii priority_queue<ii>
#define pb push_back
#define ff first
#define ss second

#define endl '\n'
const double PI = 3.141592653589793;
const double deg2rad = 0.017453292519943295;

lli modulo(lli a, lli b, lli n){
    long long x=1, y=a;
    while(b>0){
        if(b%2 ==1) x = (x*y)%n;
        y = (y*y)%n;
        b /= 2;
    }

    return x%n;
}

void solve(int n, int a[]){
    int* maxi; 
    int* mini;
    maxi= max_element(a, a+n);
    mini= min_element(a, a+n);

    if(*maxi == *mini){
        cout<<-1<<endl;
        return;
    }

    for(int i=0; i<n; i++){
        
        if(a[i] == *maxi){

            if(i!=0 && i!=n-1 && (a[i-1] != *maxi || a[i+1] != *maxi)){
                cout<<i+1<<endl;
                return;
            }

            if(i == 0 && a[i+1]!=*maxi){
                cout<<i+1<<endl;
                return;
            }
            
            if(i == n-1 && a[i-1]!=*maxi){
                cout<<i+1<<endl;
                return;
            }
        }
    }

}

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    
    int t;
    cin>>t;
    while(t--){
        int n;
        cin>>n;
        int a[n];

        for(int i=0; i<n; i++) cin>>a[i];

        solve(n, a);
    }
}
