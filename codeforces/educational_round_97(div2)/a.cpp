/*
Problem Name: ''
Author Name: Soumava Seal
Problem Source: 'Codeforces'
Date: '27th Oct' 2020'
----------------------------------------------
*/

#include<bits/stdc++.h>
using namespace std;

#define INF 10000000000
#define ii pair<int,int>
#define li long int
#define lli long long int
#define vi vector<int>
#define vii vector<ii>
#define vlli vector<lli>
#define pi priority_queue<int>
#define pii priority_queue<ii>
#define FOR(i,n) for(int i=0;i<n;i++)
#define pb push_back
#define ff first
#define ss second

#define endl '\n'
const double PI = 3.141592653589793;
const double deg2rad = 0.017453292519943295;

void solve(int l, int r){
    if(l*2>r) cout<<"YES"<<endl;
    else cout<<"NO"<<endl;
    //write your function here
}

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    int t;
    cin>>t;
    while(t--){
        // Write your code here for each testcase
        int l, r;
        cin>>l>>r;
        solve(l,r);
    }
}