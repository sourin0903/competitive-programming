# Codeforces Round #675 (Div. 2)

**Total no of problems:** 6

**Number of Solved Problems :** 0

## Index

 1. [ ] [Fence](notes/a.wiki)
 2. [ ] [Nice Matrix](notes/b.wiki)
 3. [ ] [Bargain](nots/c.wiki)
 4. [ ] [Returning Home](nots/d.wiki)
 5. [ ] [Minlexes](notes/e.wiki)
 6. [ ] [Boring Quries](notes/f.wiki)
