# Fence

### Problem Statement Summary :

 - There are 3 integers a,b,c such that >=1 && <=10^9.
 - Find d such that a non-degenerate quadrilateral can be formed
   with a,b,c,d.
        
 #### __Input :__
  - first line contains the no of testcases.
  - Each test case has only one line containing the three
    integers a,b,c.
                
 #### __Output :__ 
  - Single integer containing the value of d.

