/*
    Problem Name : 'Train Travel'
    Problem Source : "Prepbytes"
    Author :Soumava Seal
    Date :"Oct 18, 2020"
*/

#include<bits/stdc++.h>
using namespace std;

#define INF 10000000000
#define ii pair<int, int>
#define li long int
#define lli long long int
#define vi vector<int>
#define vii vector<ii>
#define vlli vector<lli>
#define pi priority_queue<int>
#define pii priority_queue<ii>
#define pb push_back
#define ff first
#define ss second

#define endl '\n'
const double PI = 3.141592653589793;
const double deg2rad = 0.017453292519943295;

lli modulo(lli a, lli b, lli n){
    long long x=1, y=a;
    while(b>0){
        if(b%2 ==1) x = (x*y)%n;
        y = (y*y)%n;
        b /= 2;
    }

    return x%n;
}

void solve(int n, int h, int a[], int t[]){
    lli count = 0, currlen=0;
    
    if(t[0]!=1) count++;

    for(int i = 1; i < n-1; i++) {
        currlen += a[i];
        
        if(t[i] == 1){
            currlen = 0;
        }else if(currlen >= h){
            count++;
            currlen = 0;
        }
    }

    if(t[n-1] != 1) count++;

    cout<<count<<endl;

}

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);

    int t;
    cin>>t;
    while(t--){
        int n, h;
        cin>>n>>h;
        int a[n], t[n];

        for(int i=0; i<n; i++) cin>>a[i];

        for(int i=0; i<n; i++) cin>>t[i];

        solve(n ,h, a, t);
    }
}

