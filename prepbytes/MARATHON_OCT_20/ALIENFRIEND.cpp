/*
    Problem Name :'My Alien Friend'
    Problem Source : "Prepbytes"
    Author :Soumava Seal
    Date :"Oct 10, 2020"
*/

#include<bits/stdc++.h>
using namespace std;

#define INF 10000000000
#define ii pair<int, int>
#define li long int
#define lli long long int
#define vi vector<int>
#define vii vector<ii>
#define vlli vector<lli>
#define pi priority_queue<int>
#define pii priority_queue<ii>
#define pb push_back
#define ff first
#define ss second

#define endl '\n'
const double PI = 3.141592653589793;
const double deg2rad = 0.017453292519943295;

lli modulo(lli a, lli b, lli n){
    long long x=1, y=a;
    while(b>0){
        if(b%2 ==1) x = (x*y)%n;
        y = (y*y)%n;
        b /= 2;
    }

    return x%n;
}

void solve(string s){
    int n = s.size();

    int count=0 ;

    for (int i = 0; i < n; i++) {
        if(s[i] == '?') count++;
    }

    lli max_possible = modulo(2,count,1000000007);
    
    vector<lli> prob;

    for(int i=0; i<n/2; i++){
        
        if(s[i]!='?' && s[i+n/2]!='?' && s[i] != s[i+n/2]){
            cout<<max_possible<<endl;
            return;
        }

        if(s[i]!='?' && s[i] == s[i+n/2]) continue;

        if(s[i]!='?' || s[i+n/2]!='?') prob.pb(1);
        else if(s[i]=='?' && s[i]==s[i+n/2]) prob.pb(2);
    }
    
    lli ans = 1;

    for (int i = 0; i < prob.size(); i++) {
        ans = (ans*prob[i])%1000000007;
    }

    cout<<max_possible - ans<<endl;
}

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    int n;
    cin>>n;
    while(n--){
        string s;
        cin>>s;
        solve(s);
    }
}

