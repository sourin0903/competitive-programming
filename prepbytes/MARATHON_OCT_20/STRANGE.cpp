/*
    Problem Name :'Strange BEhaviour'
    Problem Source : "Prepbytes"
    Author :Soumava Seal
    Date :"Oct 10, 2020"
*/

#include<bits/stdc++.h>
using namespace std;

#define INF 10000000000
#define ii pair<int, int>
#define li long int
#define lli long long int
#define vi vector<int>
#define vii vector<ii>
#define vlli vector<lli>
#define pi priority_queue<int>
#define pii priority_queue<ii>
#define pb push_back
#define ff first
#define ss second

#define endl '\n'
const double PI = 3.141592653589793;
const double deg2rad = 0.017453292519943295;

lli modulo(lli a, lli b, lli n){
    long long x=1, y=a;
    while(b>0){
        if(b%2 ==1) x = (x*y)%n;
        y = (y*y)%n;
        b /= 2;
    }

    return x%n;
}

int divisor(int n){
    int count=0, upperBound = (int)pow(n,0.5);

    for (int i = 1; i <= upperBound; i++) {
        if(n%i==0) count += 2;
    }
    
    if(upperBound*upperBound == n) count -= 1;
    
    return count;
}

void solve(int x){
    
    int d = divisor(x);
    if(d%2 == 0){
        int nxt = (int)pow(x,0.5);
        cout<<(nxt+1)*(nxt+1)<<endl;
    }else{
        cout<<x+1<<endl;
    }
}

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    int t;
    cin>>t;
    while(t--){
        int x;
        cin>>x;
        solve(x);
    }
}

