/*
    Problem Name :'City and boxes'
    Problem Source : "Prepbytes"
    Author :Soumava Seal
    Date :"Oct 14, 2020"
*/

#include<bits/stdc++.h>
using namespace std;

#define INF 10000000000
#define ii pair<int, int>
#define li long int
#define lli long long int
#define vi vector<int>
#define vii vector<ii>
#define vlli vector<lli>
#define pi priority_queue<int>
#define pii priority_queue<ii>
#define pb push_back
#define ff first
#define ss second

#define endl '\n'
const double PI = 3.141592653589793;
const double deg2rad = 0.017453292519943295;

lli modulo(lli a, lli b, lli n){
    long long x=1, y=a;
    while(b>0){
        if(b%2 ==1) x = (x*y)%n;
        y = (y*y)%n;
        b /= 2;
    }

    return x%n;
}

int a[100001] = {0};

int solve(int i, int j){
    
    if(i == 1) return a[j];

    int ans = min(solve(i-1,j), solve(i-1,j-1)) + a[j];
    return ans;
}

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    
    int n;
    cin>>n;

    for (int i = 1; i <= n; i++) {
        cin>>a[i];
    }

    int m;
    cin>>m;
    
    while(m--){
        int i,j;
        cin>>i>>j;
        cout<<solve(i,j)<<endl;
    }
}
