/*
    Problem Name : Saving with a raise background
    Problem Source : Prepbytes
    Author :Soumava Seal
    Date : October 21, 2020
*/

#include<bits/stdc++.h>
using namespace std;

#define INF 10000000000
#define ii pair<int, int>
#define li long int
#define lli long long int
#define vi vector<int>
#define vii vector<ii>
#define vlli vector<lli>
#define pi priority_queue<int>
#define pii priority_queue<ii>
#define pb push_back
#define ff first
#define ss second

#define endl '\n'
const double PI = 3.141592653589793;
const double deg2rad = 0.017453292519943295;

lli modulo(lli a, lli b, lli n){
    long long x=1, y=a;
    while(b>0){
        if(b%2 ==1) x = (x*y)%n;
        y = (y*y)%n;
        b /= 2;
    }

    return x%n;
}

void solve(float sal, float tot_cost, float por_save, float raise){
    float down_pay = 0.25*tot_cost;
    float curr_save = 0;
    float invest;

    int time = 0;

    while(curr_save < down_pay){
        time++;
        invest = curr_save*0.04/12;
        curr_save += invest + por_save*sal/12;
        
        if(time!= 0 && time%6 == 0) sal += raise*sal;
    }

    cout<<time<<endl;
}

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    
    float annual_salary, total_cost, portion_saved, raise;
    cin>>annual_salary>>total_cost>>portion_saved>>raise;

    solve(annual_salary, total_cost, portion_saved, raise);
}
