/*
    Problem Name : Parrallel Geometry.
    Problem Source : Prepbytes
    Author :Soumava Seal
    Date : October 23, 2020
*/

#include<bits/stdc++.h>
using namespace std;

#define INF 10000000000
#define ii pair<int, int>
#define li long int
#define lli long long int
#define vi vector<int>
#define vii vector<ii>
#define vlli vector<lli>
#define pi priority_queue<int>
#define pii priority_queue<ii>
#define pb push_back
#define ff first
#define ss second

#define endl '\n'
const double PI = 3.141592653589793;
const double deg2rad = 0.017453292519943295;

lli modulo(lli a, lli b, lli n){
    long long x=1, y=a;
    while(b>0){
        if(b%2 ==1) x = (x*y)%n;
        y = (y*y)%n;
        b /= 2;
    }

    return x%n;
}

void solve(int a[]){
    map<int, int> freq;

    for(int i=0; i<12; i++){
        if(freq.find(a[i]) == freq.end()) freq[a[i]] = 1;
        else freq[a[i]]++;
    } 

    for(auto itr = freq.begin(); itr!=freq.end(); itr++){
        if(itr->second % 4 != 0){
            cout<<"no"<<endl;
            return;
        }  
    }

    cout<<"yes"<<endl;

}

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    int t;
    cin>>t;
    while(t--){
        int a[12];
        for(int i=0; i<12; i++) cin>>a[i];
        solve(a);
    }
}
