/*
    Problem Name :'v-pattern'
    Problem Source : "Prepbytes"
    Author :Soumava Seal
    Date :"8th Aug'2020"
*/

#include<bits/stdc++.h>
using namespace std;

#define INF 10000000000
#define ii pair<int, int>
#define li long int
#define lli long long int
#define vi vector<int>
#define vii vector<ii>
#define vlli vector<lli>
#define pi priority_queue<int>
#define pii priority_queue<ii>
#define pb push_back
#define ff first
#define ss second

#define endl '\n'
const double PI = 3.141592653589793;
const double deg2rad = 0.017453292519943295;

lli modulo(lli a, lli b, lli n){
    long long x=1, y=a;
    while(b>0){
        if(b%2 ==1) x = (x*y)%n;
        y = (y*y)%n;
        b /= 2;
    }

    return x%n;
}

void solve(){
    int max_len = 10;
    
    for (int i = 1; i <= 5; i++) {
        for(int j=1; j<=i; j++) cout<<j;
        for(int j=0; j<max_len-2*i; j++) cout<<" ";
        for(int j=i; j>0; j--) cout<<j;
        cout<<endl;
    }
}

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    char a;
    cin>>a;
    solve();
}

