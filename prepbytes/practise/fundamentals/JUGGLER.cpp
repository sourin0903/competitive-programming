/*
    Problem Name :'Sorcerer Sequence'
    Problem Source : "Prepbytes"
    Author :Soumava Seal
    Date :"Oct 09, 2020"
*/

#include<bits/stdc++.h>
using namespace std;

#define INF 10000000000
#define ii pair<int, int>
#define li long int
#define lli long long int
#define vi vector<int>
#define vii vector<ii>
#define vlli vector<lli>
#define pi priority_queue<int>
#define pii priority_queue<ii>
#define pb push_back
#define ff first
#define ss second

#define endl '\n'
const double PI = 3.141592653589793;
const double deg2rad = 0.017453292519943295;

lli modulo(lli a, lli b, lli n){
    long long x=1, y=a;
    while(b>0){
        if(b%2 ==1) x = (x*y)%n;
        y = (y*y)%n;
        b /= 2;
    }

    return x%n;
}

void solve(int n){
    lli a = n;
    cout<<n<<" ";
    while(a>1){
        if(a%2==0) a = (lli)pow(a,0.5);
        else a = (lli)pow(a,1.5);
        cout<<a<<" ";
    }
    cout<<endl;
}

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    int t;
    cin>>t;
    while(t--){
        int n;
        cin>>n;
        solve(n);
    }
}

