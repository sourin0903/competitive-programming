#include <bits/stdc++.h>
using namespace std;

void solve(int n){
    if(n%100 == 0){
        if(n%400 == 0) cout<<"Yes"<<endl;
        else cout<<"No"<<endl;
    }else{
        if(n%4==0) cout<<"Yes"<<endl;
        else cout<<"No"<<endl;
    }
}

int main(){
    int t;
    cin>>t;
    while(t--){
        int n;
        cin>>n;
        solve(n);
    }
}
