/*
    Problem Name : 
    Problem Source : 
    Author :Soumava Seal
    Date :
*/

#include<bits/stdc++.h>
using namespace std;

#define INF 10000000000
#define ii pair<int, int>
#define li long int
#define lli long long int
#define vi vector<int>
#define vii vector<ii>
#define vlli vector<lli>
#define pi priority_queue<int>
#define pii priority_queue<ii>
#define pb push_back
#define ff first
#define ss second

#define endl '\n'
const double PI = 3.141592653589793;
const double deg2rad = 0.017453292519943295;

lli modulo(lli a, lli b, lli n){
    long long x=1, y=a;
    while(b>0){
        if(b%2 ==1) x = (x*y)%n;
        y = (y*y)%n;
        b /= 2;
    }

    return x%n;
}

int dp[21] = {0};

void solve(int a, int b, int c, int n){
    
    dp[0] = a; dp[1] = b; dp[2] = c;
    
    cout<<a<<" "<<b<<" "<<c<<" ";

    for(int i=3; i<n; i++){
        dp[i] = dp[i-1] + dp[i-2] + dp[i-3];
        cout<<dp[i]<<" ";
    }

    cout<<endl;
}

int main(){
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    
    int t;
    cin>>t;
    while(t--){
        int a, b, c, n;
        cin>>a>>b>>c>>n;
        solve(a,b,c,n);
    }
}
